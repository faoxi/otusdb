path=$(pwd)
echo $path
docker run --rm \
  -v $path/sql:/flyway/sql \
  -v $path/conf:/flyway/conf \
  flyway/flyway \
  $1