docker run -d \
  --name otuspg \
  -e POSTGRES_PASSWORD=postgres \
  --restart always \
  -p 54321:5432 \
  -v $(PWD)/data/tablespaces/ops:/tablespaces/ops  \
  -v $(PWD)/data/tablespaces/dictionary:/tablespaces/dictionary \
  -e PGDATA=/pgdata \
  -v $(PWD)/data/pgdata:/pgdata \
  postgres

SLEEP 40
CP $(PWD)/conf/pg_hba.conf $(PWD)/data/pgdata/pg_hba.conf
CP $(PWD)/conf/postgresql.conf $(PWD)/data/pgdata/postgresql.conf

docker exec otuspg psql -U postgres -c 'CREATE DATABASE shop'
docker exec otuspg psql -U postgres -c "CREATE TABLESPACE ops LOCATION '/tablespaces/ops'"
docker exec otuspg psql -U postgres -c "CREATE TABLESPACE dictionary LOCATION '/tablespaces/dictionary'"

docker restart otuspg