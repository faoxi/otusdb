-- -- -----------------------------------------------------
-- -- CUSTOMER
-- -- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS customer;
CREATE TABLE IF NOT EXISTS customer.correspondence_language (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL UNIQUE
) TABLESPACE dictionary;

CREATE TABLE IF NOT EXISTS customer.gender (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL UNIQUE
) TABLESPACE dictionary;

CREATE TABLE IF NOT EXISTS customer.marital_status (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL UNIQUE
) TABLESPACE dictionary;

CREATE TABLE IF NOT EXISTS customer.customer_status (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL UNIQUE
) TABLESPACE dictionary;

CREATE TABLE IF NOT EXISTS customer.customer (
  id UUID NOT NULL PRIMARY KEY,
  first_name VARCHAR(100) NOT NULL,
  second_name VARCHAR(100) NOT NULL,
  bith_date DATE NOT NULL,
  correspondence_language_id INT NOT NULL REFERENCES customer.correspondence_language(id),
  gender_id INT NOT NULL REFERENCES customer.gender(id),
  marital_status_id INT NOT NULL REFERENCES customer.marital_status(id),
  create_at TIMESTAMP NOT NULL,
  status INT NOT NULL REFERENCES customer.customer_status(id),
  email VARCHAR(200) NOT NULL UNIQUE
) TABLESPACE ops;


-- -- -----------------------------------------------------
-- -- ADDRESS
-- -- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS address;
CREATE TABLE IF NOT EXISTS address.country (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(300) NOT NULL
) TABLESPACE ops;

CREATE TABLE IF NOT EXISTS address.postal_code (
  id SERIAL NOT NULL PRIMARY KEY,
  code VARCHAR(20) NOT NULL UNIQUE,
  country_id INT NOT NULL REFERENCES address.country(id)
) TABLESPACE ops;

CREATE TABLE IF NOT EXISTS address.region (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(50) NOT NULL UNIQUE,
  country_id INT NOT NULL REFERENCES address.country(id)
) TABLESPACE ops;

CREATE TABLE IF NOT EXISTS address.city (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(300) NOT NULL,
  region_id INT NOT NULL REFERENCES address.region(id),
  country_id INT NOT NULL REFERENCES address.country(id)
) TABLESPACE ops;

CREATE TABLE IF NOT EXISTS address.street (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(300) NOT NULL,
  city_id INT NOT NULL REFERENCES address.city(id)
) TABLESPACE ops;

CREATE TABLE IF NOT EXISTS address.building (
  id SERIAL NOT NULL PRIMARY KEY,
  number INT NOT NULL,
  street_id INT NOT NULL REFERENCES address.street(id)
) TABLESPACE ops;

CREATE TABLE IF NOT EXISTS address.address (
  id UUID NOT NULL PRIMARY KEY REFERENCES customer.customer(id),
  country_id INT NOT NULL REFERENCES address.country(id),
  potal_code_id INT NULL REFERENCES address.postal_code(id),
  region_id INT NULL REFERENCES address.region(id),
  city_id INT NULL REFERENCES address.city(id),
  street_id INT NULL REFERENCES address.street(id),
  building_id INT NULL REFERENCES address.building(id)
) TABLESPACE ops;

-----------------------------------------------------
-- PURCHASE
-- --------------------------------------------------
CREATE SCHEMA IF NOT EXISTS purchase;
CREATE TABLE IF NOT EXISTS purchase.purchase_status (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(45) NOT NULL UNIQUE
) TABLESPACE dictionary;

CREATE TABLE IF NOT EXISTS purchase.purchase (
  id SERIAL NOT NULL PRIMARY KEY,
  created_at TIMESTAMP NOT NULL,
  purchase_status_id INT NOT NULL REFERENCES purchase.purchase_status(id),
  customer_id UUID NOT NULL REFERENCES customer.customer(id),
  cost BIGINT NOT NULL
) TABLESPACE ops;

-- -----------------------------------------------------
-- PRODUCT
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS product;

CREATE TABLE IF NOT EXISTS product.product_status (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL UNIQUE
) TABLESPACE dictionary;

CREATE TABLE IF NOT EXISTS product.product_manufacturer (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(45) NOT NULL UNIQUE
) TABLESPACE ops;

CREATE TABLE IF NOT EXISTS product.product (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL UNIQUE,
  status INT NOT NULL REFERENCES product.product_status(id),
  product_status_id INT NOT NULL REFERENCES product.product_status(id),
  product_manufacturer_id INT NOT NULL REFERENCES product.product_manufacturer(id),
  cost BIGINT NOT NULL,
  quantity INT NOT NULL,
  updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
  created_at TIMESTAMP NOT NULL DEFAULT NOW()
) TABLESPACE ops;


CREATE TABLE IF NOT EXISTS product.product_category (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(45) NOT NULL UNIQUE
) TABLESPACE dictionary;

CREATE TABLE IF NOT EXISTS product.cross_product_category (
  product_id INT NOT NULL REFERENCES product.product(id),
  product_category_id INT NOT NULL REFERENCES product.product_category(id)
)  TABLESPACE ops;


CREATE TABLE IF NOT EXISTS product.product_supplier (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(45) NOT NULL UNIQUE
) TABLESPACE ops;

CREATE TABLE IF NOT EXISTS product.product_supplier_order_status (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(100) NOT NULL UNIQUE
) TABLESPACE dictionary;


CREATE TABLE IF NOT EXISTS product.product_supplier_order (
  id SERIAL NOT NULL PRIMARY KEY,
  product_id INT NOT NULL REFERENCES product.product(id),
  product_supplier_id INT NOT NULL REFERENCES product.product_supplier(id),
  cost BIGINT NOT NULL,
  product_supplier_order_status_id INT NOT NULL REFERENCES product.product_supplier_order_status(id),
  quantity INT NULL,
  created_at TIMESTAMP NULL DEFAULT NOW()
)  TABLESPACE ops;

