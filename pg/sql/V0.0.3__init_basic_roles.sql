SET search_path TO customer,public,address,purchase,product;

-- -- -----------------------------------------------------
-- -- GROUP FOR APPLICATIONS
-- -- -----------------------------------------------------
DROP ROLE IF EXISTS app;
CREATE ROLE app;

-- Operation tables
GRANT SELECT, UPDATE, INSERT
ON TABLE customer, address, product_supplier_order,
    cross_product_category, purchase, product,
    country, postal_code, region, city,
    street, building, product_manufacturer
TO GROUP app;

-- Dictionary tables
GRANT SELECT
ON TABLE correspondence_language, gender, marital_status,
    customer_status, product_status, purchase_status,
    product_category, product_supplier, product_supplier_order_status
TO GROUP app;

-- -- -----------------------------------------------------
-- -- GROUP FOR ANALYTICS
-- -- -----------------------------------------------------
DROP ROLE IF EXISTS analytics;
CREATE ROLE analytics;
GRANT SELECT
ON TABLE correspondence_language, gender, marital_status,
    customer_status, product_status, purchase_status,
    product_category, product_supplier, product_supplier_order_status
TO GROUP analytics;
