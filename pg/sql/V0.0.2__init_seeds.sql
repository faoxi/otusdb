SET search_path TO customer,public,address,purchase,product;

INSERT INTO correspondence_language (name) VALUES('RU');
INSERT INTO correspondence_language (name) VALUES('EN');

INSERT INTO gender (name) VALUES('M');
INSERT INTO gender (name) VALUES('F');

INSERT INTO marital_status (name) VALUES('SINGLE');
INSERT INTO marital_status (name) VALUES('MARRIED');
INSERT INTO marital_status (name) VALUES('DIVORCED');
INSERT INTO marital_status (name) VALUES('COHABITING');

INSERT INTO customer_status (name) VALUES('ACTIVE');
INSERT INTO customer_status (name) VALUES ('BANNED');

INSERT INTO purchase_status (name) VALUES('ORDERED');
INSERT INTO purchase_status (name) VALUES('PREPARED');
INSERT INTO purchase_status (name) VALUES('DELIVERY');
INSERT INTO purchase_status (name) VALUES('WAITING_CUSTOMER');
INSERT INTO purchase_status (name) VALUES('DONE');

INSERT INTO product_category (name)VALUES ('BOOKS');
INSERT INTO product_category (name)VALUES ('ONLINE_BOOKS');
INSERT INTO product_category (name) VALUES('TECH');

INSERT INTO product_manufacturer (name) VALUES('SAFARI_BOOKS');
INSERT INTO product_manufacturer (name) VALUES('AMAZON');

INSERT INTO product_status (name) VALUES('WAITING_DELIVERY');
INSERT INTO product_status (name) VALUES('READY');
INSERT INTO product_status (name) VALUES('NOT_AVAILABLE');


INSERT INTO product.product_supplier (name) VALUES('HORNS_AND_HOOVES');
INSERT INTO product.product_supplier (name) VALUES('MY_MIND_COMPANY');


